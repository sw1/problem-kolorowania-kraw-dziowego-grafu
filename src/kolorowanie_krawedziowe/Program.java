package kolorowanie_krawedziowe;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import edu.uci.ics.jung.graph.Graph;

public class Program {
	GraphGUI window;
	Population population;
	Graph <Integer,Integer> g=null;
	Integer iterations=100;
	int degree;
	
	Program(){
		window=new GraphGUI();
		window.getBtnGeneruj().addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseClicked(MouseEvent e) {
				generateGraph();
			}
		});
		window.getBtnColorize().addMouseListener(new MouseListener() {
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseClicked(MouseEvent e) {
				colorize();
			}
		});
		
	}
	
	
	void generateGraph(){
		GraphGeneratorHelper generator=new GraphGeneratorHelper(window.getVertexCount(), 100);
		g=generator.generate();
		window.setColoring(null);
		window.drawGraph(g);
		
		degree=0;
		int score;
		for(Integer vertex:g.getVertices()){
			score=g.degree(vertex);
			if(score>degree){
				degree=score;
			}
		}
		window.setFirstPopulationColorCount(degree);
		window.setText("Edges: "+g.getEdgeCount()+" degree: "+degree);
	}
	
	void colorize(){
		if(g==null)
			return;
		ArrayList<Integer> plotDataBest=new ArrayList<Integer>(iterations);
		ArrayList<Integer> plotDataAvarage=new ArrayList<Integer>(iterations);
		ArrayList<Integer> plotDataWorst=new ArrayList<Integer>(iterations);
		population = new Population(g);
		iterations=window.getIterations();
		population.setPopulationCount(window.getPopulationCount());
		population.setMutationRate(window.getMuationRate());
		population.setMutationType(window.getMutationType());
		population.setFirstPopulationColorCount(window.getFirstPopulationColorCount());
		
		Chromosome.maxVertexDegree=degree;
		population.generateRandomPopulation();
		int i;
		for(i = 0; i<iterations; i++){
			population.sortPopulation();
			plotDataBest.add(population.bestColoring().getPenaltyFactor());
			plotDataAvarage.add(population.middleColoring().getPenaltyFactor());
			plotDataWorst.add(population.worstColoring().getPenaltyFactor());
			if(population.bestColoring().getPenaltyFactor()==degree)
				break;
			//System.out.println("----------");
			//for(Chromosome ch:population.chromosomes){
			//Chromosome ch=population.bestColoring();
			//	System.out.println("Chromosome -------");
			//	ch.print();
			//}
			population.crossover();	
			population.mutate();
		}
		String str=String.format("<html>Edges: %d degree %d iterations: %d<br>Used colors:%d bad colored:%d factor: %d</html>",g.getEdgeCount(),degree,i-1, population.bestColoring().getColorsNumber(),  population.bestColoring().getBadPairsNumber(), population.bestColoring().getPenaltyFactor());
		window.setColoring(population.bestColoring());
		window.drawGraph(g);
		window.setText(str);
		PlotWindow plot=new PlotWindow(plotDataBest,plotDataAvarage,plotDataWorst, i);
		plot.initialize();
	}
	
	
	public static void main(String[] args){

		Program prg=new Program();
		//System.out.println("Bad pairs: "+population.bestColoring().getBadPairsNumber());
			
		
	}
	
	
	
	
	
}
