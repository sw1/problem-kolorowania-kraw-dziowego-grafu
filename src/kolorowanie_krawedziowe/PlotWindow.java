package kolorowanie_krawedziowe;

import java.util.ArrayList;

import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class PlotWindow {
	private JFrame frame;
	private ArrayList<Integer> data;
	private Integer count;
	private ArrayList<Integer> dataBest;
	private ArrayList<Integer> dataAvarage;
	private ArrayList<Integer> dataWorst;
	
	PlotWindow(ArrayList<Integer> dataBest,ArrayList<Integer> dataAvarage,ArrayList<Integer> dataWorst, Integer count){
		this.dataBest=dataBest;
		this.dataAvarage=dataAvarage;
		this.dataWorst=dataWorst;
		this.count=count;
		
	}
	
	public void initialize(){
		frame=new JFrame();
		frame.setBounds(100, 100, 640, 480);
		
		final ArrayList<XYSeries> series = new ArrayList<XYSeries>();
		series.add(new XYSeries("Best"));
		series.add(new XYSeries("Avarage"));
		series.add(new XYSeries("Worst"));
		for(int i=0;i<count;i++){
			series.get(0).add(i,dataBest.get(i));
			series.get(1).add(i,dataAvarage.get(i));
			series.get(2).add(i,dataWorst.get(i));
		}		
		XYPlot plot = new XYPlot();
		plot.setRangeAxis(new NumberAxis("Penalty factor"));
		NumberAxis domain=new NumberAxis("iterations");
		plot.setDomainAxis(domain);
		final XYSeriesCollection set1=new XYSeriesCollection(series.get(0));
		final XYSeriesCollection set2=new XYSeriesCollection(series.get(1));
		final XYSeriesCollection set3=new XYSeriesCollection(series.get(2));
		plot.setDataset(0, set1);
		plot.setRenderer(0, new XYLineAndShapeRenderer());
		plot.setDataset(1, set2);
		plot.setRenderer(1, new XYLineAndShapeRenderer());
		plot.setDataset(2, set3);
		plot.setRenderer(2, new XYLineAndShapeRenderer());
		/*final JFreeChart chart = ChartFactory.createXYLineChart(
		        "Adaptation plot",
		        "iterations", 
		        "colors count", 
		        coldata,
		        PlotOrientation.VERTICAL,
		        true,
		        true,
		        false
		    );*/
		final JFreeChart chart=new JFreeChart("Adaptation plot",plot);
		final ChartPanel chartPanel = new ChartPanel(chart);
	    chartPanel.setPreferredSize(new java.awt.Dimension(640, 480));
	    frame.setContentPane(chartPanel);
	    frame.setVisible(true);
	}
}
