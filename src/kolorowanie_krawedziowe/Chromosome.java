package kolorowanie_krawedziowe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import org.apache.commons.collections15.functors.InstanceofPredicate;

import com.orsoncharts.graphics3d.Offset2D;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.util.Pair;

public class Chromosome {

	// skĹ‚adowe indywidualne dla kaĹĽdej instancji chromosomu:
	HashMap<Integer, ArrayList<Integer>> chGroups; // [index=kolor,
													// chGroups[index]=lista
													// krawÄ™dzi tego koloru]
	// reprezentacja odpowiednia do krzyĹĽowania i mutowania
	// oraz sprawdzenia liczby
	// wykorzystanych kolorĂłw

	HashMap<Integer, Integer> chEdges; // [index=numer krawÄ™dzi,
										// chEdges[index]=kolor dla tej
										// krawÄ™dzi]
	// reprezentacja potrzebna do okreĹ›lenia poprawnoĹ›ci kolorowania
	// (zmniejsza nakĹ‚ady czasowe przy przeszukiwaniach,
	// poniewaĹĽ nie musimy przeszukiwaÄ‡ kaĹĽdego koloru, aby
	// onaleĹşÄ‡ danÄ… krawÄ™dĹş)

	int badlyColouredPairsNum;
	int usedColorsNum;

	// skĹ‚adowe klasy (zmienne statyczne), identyczne dla kaĹĽdego chromosomu w
	// populacji
	// (inicjalizowane gdzieĹ› odgĂłrnie, np przy zakĹ‚adaniu populacji)
	static Graph<Integer, Integer> g; // referencja do struktury grafu, dla
										// ktĂłrego szukamy optymalnego
										// kolorowania
	static Collection<Integer> vertices; // kolekcja wierzchoĹ‚kĂłw tego grafu
	static int vertexNumber;
	static int edgesCount;
	static int maxVertexDegree; // najwyĹĽszy stopieĹ„ wierzchoĹ‚ka w grafie
	// statyczne zmienne pomocnicze:
	// patrz: paintChromosome(), getBadPairsNumber()
	static Collection<Integer> adjEdges; // przechowuje kolekcje krawÄ™dzi
											// wychodzÄ…cych z jednego
											// wierzchoĹ‚ka
	static int[] edgesToColors; // [index=kolor, edgesToColors[index]=wartoĹ›c
								// wskazujÄ…ca na jego dostÄ™pnoĹ›Ä‡]
								// tablica potrzebna kontroli poprawnoĹ›ci
								// chromosoma

	static int firstPopulationColorCount=0;
	
	public Chromosome() {

		chEdges = new HashMap<Integer, Integer>();
		// edgesToColors = new int[edgesNumber]; TO POWINNO BYÄ† INICJALIZOWANE
		// PRZY ZAKĹ�ADANIU POPULACJI
		chGroups = new HashMap<Integer, ArrayList<Integer>>();
		/*
		 * for (int i = 0; i < edgesCount; i++) { chGroups.add(new
		 * ColorGroup()); }
		 */
		badlyColouredPairsNum = 0;
		usedColorsNum = 0;

	}

	HashMap<Integer, Integer> putEdgesToColor() {
		chEdges = new HashMap<Integer, Integer>();
		for (Integer color : chGroups.keySet()) {
			for (Integer edge : chGroups.get(color)) {
				chEdges.put(edge, color);
			}
		}
		return chEdges;
	}
	
	void paintChromosome2() {
		if(firstPopulationColorCount==0) firstPopulationColorCount=g.getEdgeCount();
		int color=0;
		Random generator = new Random();
		for (Integer v : vertices) {
			adjEdges = g.getIncidentEdges(v);
			for (int edge : adjEdges){
				color=generator.nextInt(firstPopulationColorCount);
				if(!chGroups.containsKey(color))
					chGroups.put(color,new ArrayList<Integer>());
				chGroups.get(color).add(edge);
			}
		}
		
	}
	void paintChromosome() {

		Random generator = new Random();
		int color = -1;

		for (Integer v : vertices) {
			Arrays.fill(edgesToColors, -1);
			adjEdges = g.getIncidentEdges(v); // it will return collection of
												// edges in this graph

			for (Integer edge : adjEdges) { // pierwsze przejĹ›cie po
											// krawÄ™dziach -
											// wklepanie do edgesToColors
				// nadanych wczeĹ›niej kolorĂłw
				if (chEdges.containsKey(edge)) {// true, gdy krawÄ™dĹş ma juz
												// nadany kolor
					color = chEdges.get(edge);
					edgesToColors[color] = edge;
				}
			}
			for (Integer edge : adjEdges) { // drugie przejĹ›cie; nadanie
											// kolorĂłw
				// pozostaĹ‚ym sierotom
				if (!chEdges.containsKey(edge)) {
					do {
						color = generator.nextInt(edgesCount);
					} while (edgesToColors[color] != -1);// losuj aĹĽ znajdziesz
															// wolny kolor
				}
				edgesToColors[color] = edge; // przypisanie wylosowanemu
												// kolorkowu
												// numeru krawÄ™dzi
			}

			for (int col = 0; col < edgesToColors.length; col++) { // przejĹ›cie
																	// po
																	// tablicy
																	// kolorowaĹ„
																	// i
																	// zapisanie
																	// wynikĂłw
				if ((edgesToColors[col] != -1)
						&& (!chEdges.containsKey(edgesToColors[col]))) {
					chEdges.put(edgesToColors[col], col); // reprezentacja
															// krawÄ™dzie ->
															// kolory
					if(chGroups.get(col)==null)
						chGroups.put(col, new ArrayList<Integer>());
					chGroups.get(col).add(edgesToColors[col]); // reprezentacja
																// kolory(grupy)
																// -> listy
																// krawÄ™dzi
				}
			}

		}
		usedColorsNum = getColorsNumber();
	}

	public int getPenaltyFactor() {
		// zwraca :
		// iloĹ›Ä‡ kolorĂłw wykorzystanych do pokolorowania grafu (iloĹ›Ä‡
		// niepustych
		// list w ch)
		// dodatkowo, jeĹ›li graf jest niepoprawny:
		// + najwyĹĽszy stopieĹ„ wierzchoĹ‚ka + liczba par krawÄ™dzi, ktĂłre sÄ…
		// Ĺşle
		// pokolorowane
		// (majÄ… ten sam wierzchoĹ‚ek i ten sam kolor)
		putEdgesToColor();
		badlyColouredPairsNum = getBadPairsNumber();
		return getColorsNumber()
				+ (badlyColouredPairsNum > 0 ? (edgesCount + badlyColouredPairsNum)
						: 0);
	}

	public int getColorsNumber() {
		return chGroups.size();
	}

	public int getBadPairsNumber() {
		int badPairs = 0;

		for (Integer v : vertices) {
			adjEdges = g.getIncidentEdges(v); // it will return collection of
												// edges in this graph which are
												// connected to vertex v

			for (int i = 0; i < edgesToColors.length; i++) {
				edgesToColors[i] = 0;
			}

			for (Integer e : adjEdges) {
				if (chEdges.get(e) != null)
					edgesToColors[chEdges.get(e)]++;
				// dla kaĹĽdej krawÄ™dzi e iterujemy tablicÄ™ vertToColors pod
				// indeksem koloru
				// ktĂłry posiada ta krawÄ™dĹş w naszym kolorowaniu
				// tutaj wykorzystujemy reprezentacjÄ™ chEdges
				// (Ĺ‚atwy dostÄ™p do koloru dla danej krawÄ™dzi)
			}

			for (int i = 0; i < edgesToColors.length; i++) {
				badPairs += (edgesToColors[i] * (edgesToColors[i] - 1)) / 2;
				// jeĹ›li n to liczba krawÄ™dzi o tym samym kolorze,
				// wychodzÄ…cych z jednego werzchoĹ‚ka
				// to liczba Ĺşle pokolorowanych par w tym wierzchoĹ‚ku jest
				// rĂłwna
				// (n(n-1))/2 (n po 2)
			}

		}

		return badPairs;
	}

	void removeDuplicate(List<Integer> chosenColors, Chromosome child) {
		for (Integer color: child.chGroups.keySet()) {
			for (ListIterator<Integer> iter = child.chGroups.get(color)
					.listIterator(); iter.hasNext();) {
				Integer edge = iter.next();
				if (!child.chEdges.containsKey(edge)) {
					child.chEdges.put(edge, color);
				} else {
					Integer curEdgeColor=child.chEdges.get(edge);
					if(!chosenColors.contains(curEdgeColor)){
						if(color==curEdgeColor)
							iter.remove();
						else
							child.chGroups.get(curEdgeColor).remove(edge);
						child.chEdges.put(edge, color);

					}else{
						iter.remove();
					}
				}
			}
		}
	}

	/**
	 * Takes second parent as parametr and creates child. Colors which are
	 * duplicated will be taken from second parent
	 * 
	 * @return child
	 */
	public Chromosome crossover(Chromosome parent) {
		Chromosome child = new Chromosome();
		Random rand = new Random();
		// select which groups are to be swapped from this chromosome by parent
		// chromosome
		ArrayList<Integer> colorList=new ArrayList<Integer>(parent.chGroups.keySet());
		int groupCount = colorList.size();
		final int start = rand.nextInt(groupCount);
		final int end = rand.nextInt(groupCount - start) + start;
		List<Integer> chosenColors=colorList.subList(start, end);
		for (int color:chosenColors) {
			child.chGroups.put(color,
					new ArrayList<Integer>(parent.chGroups.get(color)));
		}
		for (int color : this.chGroups.keySet()) {
			if (!child.chGroups.containsKey(color))
				child.chGroups.put(color,
						new ArrayList<Integer>(this.chGroups.get(color)));
		}

		/*
		 * Deduplication of edges. If one edge is colored two times the color
		 * from second parent is chosen. It fills chEdges array
		 */

		/*for (int color = start; color < end; color++) {
			for (Integer edge : child.chGroups.get(color)) {
				if (!child.chEdges.containsKey(edge))
					child.chEdges.put(edge, color);
			}
		}*/

		removeDuplicate(chosenColors, child);
		// Look for edges without color, and find first appopriate color
		for (int edge : g.getEdges()) {
			if (!child.chEdges.containsKey(edge)) {
				for (int color :child.chGroups.keySet()) {
						// Check if color is avaible
						Pair<Integer> endPoints = g.getEndpoints(edge);
						if (checkIfColorIsUnused(endPoints.getFirst(), child,
								color)
								&& checkIfColorIsUnused(endPoints.getSecond(),
										child, color)) {
							child.chGroups.get(color).add(edge);
							child.chEdges.put(edge, color);
							break;
						}
				}
				// If still no color is set select random color
				if (!child.chEdges.containsKey(edge)) {
					Integer color;
					color = rand.nextInt(edgesCount/2);
					if(!child.chGroups.containsKey(color))
						child.chGroups.put(color,new ArrayList<Integer>());
					child.chGroups.get(color).add(edge);
					child.chEdges.put(edge, color);
				}
			}
		}
		removeEmpty();
		return child;
	};

	/**
	 * Search for color in incident edges, if color isn't found then return true
	 */
	private static boolean checkIfColorIsUnused(Integer vertex, Chromosome ch,
			Integer color) {
		Collection<Integer> edges = g.getIncidentEdges(vertex);
		for (Integer edgeNum : edges) {
			if (ch.chEdges.get(edgeNum) == color) {
				return false;
			}
		}
		return true;
	}

	public void mutate2() {

		Random generator = new Random();
		Integer mutatedColor;
		Integer mutatedEdge;
		Integer nextGroup;
		ArrayList<Integer> colors=new ArrayList<Integer>(chGroups.keySet());
		mutatedColor = generator.nextInt(colors.size());
		List<Integer> mutatedGroup = new ArrayList<Integer>(chGroups.get(colors.get(mutatedColor)));
		chGroups.remove(mutatedColor);
		colors.remove(mutatedColor);
		//for(Integer edge:mutatedGroup){
		//	chGroups.get(colors.get(generator.nextInt(colors.size()))).add(edge);
		//}
		chGroups.get(colors.get(generator.nextInt(colors.size()))).addAll(mutatedGroup);
	}

	public void mutate() {

		// funkcja mutate() losuje jeden z uzywanych w grafie kolorĂłw i
		// rozlokowuje jego krawÄ™dzie do innych uĹĽywanych kolorĂłw
		// kiedy nie istnieje kolor, ktĂłry mĂłgĹ‚by jÄ… przyjÄ…Ä‡, krawÄ™dĹş
		// pozostaje w swojej pierwotnej grupie
		// (mutate nie naprawia niepoprawnych grafĂłw)
		Random generator = new Random();
		Integer mutatedColor;
		List<Integer> mutatedEdges;
		ArrayList<Integer> failedToMutateEdges = new ArrayList<Integer>();
		Pair<Integer> edgeEndpoints;

		ArrayList<Integer> colors=new ArrayList<Integer>(chGroups.keySet());
		// losuj aĹĽ znajdziesz niepusty kolor do zmutowania

		mutatedColor = colors.get(generator.nextInt(colors.size()));
		mutatedEdges = chGroups.get(mutatedColor);

		for (Integer me : mutatedEdges) { // dla kaĹĽdej krawÄ™dzi z mutowanego
											// koloru...

			// wyczyszczenie tablicy kolorĂłw
			for (int i = 0; i < edgesToColors.length; i++) {
				edgesToColors[i] = 0;
			}

			// wyĹ‚aczenie z rozwaĹĽaĹ„ mutowanego koloru (tam nie chcemy
			// wrzucaÄ‡)
			edgesToColors[mutatedColor] = -1;

			// wyĹ‚Ä…czenie z moĹĽliwych kolorĂłw tych, w ktĂłrych sa juz
			// ulokowane sÄ…siednie krawÄ™dzie
			// dla kaĹĽdego z koĹ„cowych wierzchoĹ‚kĂłw...
			edgeEndpoints = g.getEndpoints(me);
			Iterator<Integer> itr = edgeEndpoints.iterator(); // CZY TEN
																// ITERATOR JEST
																// OK???????
			while (itr.hasNext()) {
				adjEdges = g.getIncidentEdges((Integer) itr.next());
				for (Integer e : adjEdges) {
					edgesToColors[chEdges.get(e)] = -1;
				}
			}

			// przydzielenie nowego koloru dla krawÄ™dzi
			boolean found = false;
			for (int i = 0; i < edgesToColors.length; i++) {
				if (edgesToColors[i] != -1 && chGroups.containsKey(i)) { // jeĹ›li
																			// kolor
																			// jest
																			// wolny
																			// i
																			// niepusty
					chGroups.get(i).add(me);
					chEdges.put(me, i);
					found = true;
					break;
				}
			}
			if (!found)
				failedToMutateEdges.add(me);
		}

		// ustawienie nowej listy krawÄ™dzi mutowanego koloru
		// we hope its gonna be empty sialalala
		chGroups.put(mutatedColor, failedToMutateEdges);

	};

	private void removeEmpty() {
		for (Iterator<ArrayList<Integer>> iter=chGroups.values().iterator();iter.hasNext();) {
			ArrayList<Integer> lst=iter.next();
			if(lst.isEmpty())
				iter.remove();
		}
	}

	public void print() {
		for (int color :chGroups.keySet()) {
				System.out.print("Color " + Integer.toString(color) + ": ");
				for (int edge : chGroups.get(color)) {
					System.out.print(Integer.toString(edge) + " ");
				}
				System.out.println();
		}

	}
}
