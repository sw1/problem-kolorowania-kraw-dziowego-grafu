package kolorowanie_krawedziowe;

import org.apache.commons.collections15.Factory;

import edu.uci.ics.jung.algorithms.generators.GraphGenerator;
import edu.uci.ics.jung.algorithms.generators.random.ErdosRenyiGenerator;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;

public class GraphGeneratorHelper {
	private GraphGenerator<Integer, Integer> generator;
	private int vertexCount;
	private int edgesCount;

	public GraphGeneratorHelper(int vertexCount, int edgesCount) {
		this.vertexCount = vertexCount;
		this.edgesCount = edgesCount;
	}

	class IntegerFactory implements Factory<Integer>{
		int count = 0;
		public Integer create() {
			return new Integer(count++);
		}
	}
	
	class GraphFactory implements Factory<UndirectedGraph<Integer, Integer>> {
		public UndirectedGraph<Integer, Integer> create() {
			return new UndirectedSparseMultigraph<Integer, Integer>();
		}
	}
	
	public Graph<Integer, Integer> generate() {
		Factory<Integer> vertexFactory = new IntegerFactory();
		Factory<Integer> edgesFactory = new IntegerFactory();
		Factory<UndirectedGraph<Integer, Integer>> graphFactory = new GraphFactory();
		UndirectedSparseGraph<Integer, Integer> graph=new UndirectedSparseGraph<Integer,Integer>();
		generator = new ErdosRenyiGenerator<Integer, Integer>(graphFactory
				, vertexFactory, edgesFactory, vertexCount,
				0.5);
		return generator.create();
	}
}
