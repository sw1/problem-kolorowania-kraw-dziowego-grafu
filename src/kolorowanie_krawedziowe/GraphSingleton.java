package kolorowanie_krawedziowe;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;

public class GraphSingleton {
	
	   private static GraphSingleton instance = null;
	   Graph<Integer,Integer> g;
	   
	   protected GraphSingleton() {
	      // Exists only to defeat instantiation.
		   g=new SparseMultigraph<Integer,Integer>();
	   }
	   public static GraphSingleton getInstance() {		  
	      if(instance == null) {	    	  
	         instance = new GraphSingleton(); //called only once!
	         instance.generate15EdgesGraph();
	      }
	      return instance;
	   }
	   
	   private void generate15EdgesGraph(){
		   g.addVertex((Integer)0);
		   g.addVertex((Integer)1);
		   g.addVertex((Integer)2); 
		   g.addVertex((Integer)3);
		   g.addVertex((Integer)4);
		   g.addVertex((Integer)5); 
		   g.addVertex((Integer)6);
		   g.addVertex((Integer)7);
		   g.addVertex((Integer)8); 
		   g.addVertex((Integer)9);

		   g.addEdge((Integer)0, 0, 1);
		   g.addEdge((Integer)1, 0, 3);
		   g.addEdge((Integer)2, 1, 3);
		   g.addEdge((Integer)3, 1, 6);
		   g.addEdge((Integer)4, 2, 3);
		   g.addEdge((Integer)5, 2, 8);
		   g.addEdge((Integer)6, 3, 4);
		   g.addEdge((Integer)7, 3, 5);
		   g.addEdge((Integer)8, 4, 5);
		   g.addEdge((Integer)9, 5, 8);
		   g.addEdge((Integer)10, 5, 9);
		   g.addEdge((Integer)11, 6, 7);
		   g.addEdge((Integer)12, 6, 9);
		   g.addEdge((Integer)13, 7, 9);
		   g.addEdge((Integer)14, 8, 9);		   		   
	   }
	}
