package kolorowanie_krawedziowe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Paint;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JFrame;

import java.awt.GridLayout;

import javax.swing.JInternalFrame;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

import java.awt.GridBagConstraints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

import javax.swing.JLabel;

import java.awt.Insets;
import java.util.HashMap;
import java.util.Random;

import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JPanel;

public class GraphGUI {

	private JFrame frame;
	private JButton btnGeneruj;
	private JButton btnColorize;
	private JSpinner spinner;
	private JComboBox<String> comboBox;
	private Chromosome coloring;
	private BasicVisualizationServer<Integer,Integer> vv;
	private JLabel lblLabel;
	private JLabel lblPolpulationCount;
	private JSpinner spinner_1;
	private JLabel lblMaxIterations;
	private JSpinner spinner_2;
	private JLabel lblMuatations;
	private JSpinner spinner_3;
	private JLabel lblFirstPopulationcolor;
	private JSpinner spinner_4;
	/**
	 * Create the application.
	 */
	public GraphGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1024, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		btnGeneruj = new JButton("Generate");
		GridBagConstraints gbc_btnGeneruj = new GridBagConstraints();
		gbc_btnGeneruj.insets = new Insets(0, 0, 5, 5);
		gbc_btnGeneruj.anchor = GridBagConstraints.WEST;
		gbc_btnGeneruj.gridx = 2;
		gbc_btnGeneruj.gridy = 0;
		frame.getContentPane().add(btnGeneruj, gbc_btnGeneruj);
		
		JLabel lblVertexCount = new JLabel("Vertex count:");
		GridBagConstraints gbc_lblVertexCount = new GridBagConstraints();
		gbc_lblVertexCount.anchor = GridBagConstraints.WEST;
		gbc_lblVertexCount.insets = new Insets(0, 0, 5, 5);
		gbc_lblVertexCount.gridx = 0;
		gbc_lblVertexCount.gridy = 0;
		frame.getContentPane().add(lblVertexCount, gbc_lblVertexCount);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(20, 10, 300, 1));
		GridBagConstraints gbc_spinner = new GridBagConstraints();
		gbc_spinner.anchor = GridBagConstraints.WEST;
		gbc_spinner.insets = new Insets(0, 0, 5, 5);
		gbc_spinner.gridx = 1;
		gbc_spinner.gridy = 0;
		frame.getContentPane().add(spinner, gbc_spinner);
		
		btnColorize = new JButton("Colorize!");
		GridBagConstraints gbc_btnColorize = new GridBagConstraints();
		gbc_btnColorize.insets = new Insets(0, 0, 5, 5);
		gbc_btnColorize.gridx = 3;
		gbc_btnColorize.gridy = 0;
		frame.getContentPane().add(btnColorize, gbc_btnColorize);
		
		JLabel lblMethod = new JLabel("Method:");
		GridBagConstraints gbc_lblMethod = new GridBagConstraints();
		gbc_lblMethod.insets = new Insets(0, 0, 5, 5);
		gbc_lblMethod.anchor = GridBagConstraints.EAST;
		gbc_lblMethod.gridx = 4;
		gbc_lblMethod.gridy = 0;
		frame.getContentPane().add(lblMethod, gbc_lblMethod);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"genetic", "normal"}));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.anchor = GridBagConstraints.WEST;
		gbc_comboBox.gridx = 5;
		gbc_comboBox.gridy = 0;
		frame.getContentPane().add(comboBox, gbc_comboBox);
		
		lblLabel = new JLabel("");
		GridBagConstraints gbc_lblLabel = new GridBagConstraints();
		gbc_lblLabel.anchor = GridBagConstraints.WEST;
		gbc_lblLabel.gridwidth = 2;
		gbc_lblLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblLabel.gridx = 6;
		gbc_lblLabel.gridy = 0;
		frame.getContentPane().add(lblLabel, gbc_lblLabel);
		
		lblPolpulationCount = new JLabel("Population count");
		GridBagConstraints gbc_lblPolpulationCount = new GridBagConstraints();
		gbc_lblPolpulationCount.anchor = GridBagConstraints.EAST;
		gbc_lblPolpulationCount.insets = new Insets(0, 0, 5, 5);
		gbc_lblPolpulationCount.gridx = 0;
		gbc_lblPolpulationCount.gridy = 1;
		frame.getContentPane().add(lblPolpulationCount, gbc_lblPolpulationCount);
		
		spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(50, 10, 200, 1));
		GridBagConstraints gbc_spinner_1 = new GridBagConstraints();
		gbc_spinner_1.anchor = GridBagConstraints.WEST;
		gbc_spinner_1.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_1.gridx = 1;
		gbc_spinner_1.gridy = 1;
		frame.getContentPane().add(spinner_1, gbc_spinner_1);
		
		lblMaxIterations = new JLabel("Max iterations:");
		GridBagConstraints gbc_lblMaxIterations = new GridBagConstraints();
		gbc_lblMaxIterations.anchor = GridBagConstraints.EAST;
		gbc_lblMaxIterations.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxIterations.gridx = 2;
		gbc_lblMaxIterations.gridy = 1;
		frame.getContentPane().add(lblMaxIterations, gbc_lblMaxIterations);
		
		spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerNumberModel(100, 1, 1000, 1));
		GridBagConstraints gbc_spinner_2 = new GridBagConstraints();
		gbc_spinner_2.anchor = GridBagConstraints.WEST;
		gbc_spinner_2.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_2.gridx = 3;
		gbc_spinner_2.gridy = 1;
		frame.getContentPane().add(spinner_2, gbc_spinner_2);
		
		lblMuatations = new JLabel("Muatations:");
		GridBagConstraints gbc_lblMuatations = new GridBagConstraints();
		gbc_lblMuatations.anchor = GridBagConstraints.EAST;
		gbc_lblMuatations.insets = new Insets(0, 0, 5, 5);
		gbc_lblMuatations.gridx = 4;
		gbc_lblMuatations.gridy = 1;
		frame.getContentPane().add(lblMuatations, gbc_lblMuatations);
		
		spinner_3 = new JSpinner();
		spinner_3.setModel(new SpinnerNumberModel(0.2, 0.0, 1.0, 0.1));
		GridBagConstraints gbc_spinner_3 = new GridBagConstraints();
		gbc_spinner_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner_3.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_3.gridx = 5;
		gbc_spinner_3.gridy = 1;
		frame.getContentPane().add(spinner_3, gbc_spinner_3);
		
		lblFirstPopulationcolor = new JLabel("<html>First population <br>color count</html>");
		GridBagConstraints gbc_lblFirstPopulationcolor = new GridBagConstraints();
		gbc_lblFirstPopulationcolor.anchor = GridBagConstraints.EAST;
		gbc_lblFirstPopulationcolor.insets = new Insets(0, 0, 5, 5);
		gbc_lblFirstPopulationcolor.gridx = 6;
		gbc_lblFirstPopulationcolor.gridy = 1;
		frame.getContentPane().add(lblFirstPopulationcolor, gbc_lblFirstPopulationcolor);
		
		spinner_4 = new JSpinner();
		spinner_4.setModel(new SpinnerNumberModel(new Integer(30), new Integer(5), null, new Integer(1)));
		GridBagConstraints gbc_spinner_4 = new GridBagConstraints();
		gbc_spinner_4.anchor = GridBagConstraints.WEST;
		gbc_spinner_4.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_4.gridx = 7;
		gbc_spinner_4.gridy = 1;
		frame.getContentPane().add(spinner_4, gbc_spinner_4);
		frame.setVisible(true);
	}

	class ColorTransformer implements Transformer<Integer, Paint>{

		HashMap<Integer,Integer> edgesToColor;
		Integer colorCount;
		Integer usedColors=0;
		HashMap<Integer,Color> colorToRGB=new HashMap<Integer,Color>();
		public ColorTransformer() {
			colorCount=coloring.getColorsNumber();
			edgesToColor=coloring.putEdgesToColor();
			for(int color:edgesToColor.values()){
				if(!colorToRGB.containsKey(color)){
					colorToRGB.put(color,new Color(usedColors*(0x1000000/colorCount)));
					usedColors++;
				}
			}
		}
		
		@Override
		public Paint transform(Integer arg0) {
			return colorToRGB.get(edgesToColor.get(arg0));
		}
		
	}
	public void drawGraph(Graph<Integer, Integer> g){
		Layout<Integer, Integer> layout = new CircleLayout<Integer, Integer>(g);
		layout.setSize(null); // sets the initial size of the space
		if(vv!=null)
			frame.getContentPane().remove(vv);
		vv =
		new BasicVisualizationServer<Integer,Integer>(layout);
		if(coloring!=null){
			Transformer<Integer, Paint> edgePaintTransformer =new ColorTransformer();
			vv.getRenderContext().setEdgeDrawPaintTransformer(edgePaintTransformer);
		}
		vv.setPreferredSize(null); //Sets the viewing area size
		vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Integer>());
		vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<Integer>());
		vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);
		
		GridBagConstraints gbc_vv = new GridBagConstraints();
		gbc_vv.gridwidth = 11;
		gbc_vv.insets = new Insets(0, 0, 0, 5);
		gbc_vv.fill = GridBagConstraints.BOTH;
		gbc_vv.gridx = 0;
		gbc_vv.gridy = 2;
		frame.getContentPane().add(vv,gbc_vv);
		frame.setVisible(true);
	}
	public JButton getBtnGeneruj() {
		return btnGeneruj;
	}
	public JButton getBtnColorize() {
		return btnColorize;
	}
	public Integer getVertexCount() {
		return (Integer) spinner.getValue();
	}
	
	public String getMethod() {
		return (String) comboBox.getSelectedItem();
	}
	public void setColoring(Chromosome coloring){
		this.coloring=coloring;
	}
	public void setText(String text){
		lblLabel.setText(text);
	}
	public Integer getPopulationCount(){
		return (Integer) spinner_1.getValue();
	}
	
	public Integer getMutationType(){
		return comboBox.getSelectedIndex();
	}
	
	public Double getMuationRate(){
		return (Double) spinner_3.getValue();
	}

	public Integer getIterations(){
		return (Integer) spinner_2.getValue();
	}
	public Integer getFirstPopulationColorCount() {
		return (Integer) spinner_4.getValue();
	}
	
	public void setFirstPopulationColorCount(Integer count) {
		spinner_4.setValue(count);
	}
}
