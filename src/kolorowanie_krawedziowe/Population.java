package kolorowanie_krawedziowe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import edu.uci.ics.jung.graph.Graph;

public class Population {
	Graph<Integer, Integer> graph;
	ArrayList<Chromosome> chromosomes=new ArrayList<Chromosome>();
	ArrayList<Integer> grades;
	Random rand = new Random();
	int populationCount=50;
	double mutationRate=0.2;
	private int mutationCount;
	private Integer mutationType=0;
	
	public void setMutationType(Integer type){
		mutationType=type;
	}
	public void setPopulationCount(int populationCount){
		this.populationCount=populationCount;
	}
	
	public void setMutationRate(double mutationRate){
		this.mutationRate=mutationRate;
		mutationCount=(int)(populationCount*mutationRate);
	}
	
	public Population(Graph<Integer, Integer> graph) {
		this.graph = graph;
		Chromosome.g=graph;
		Chromosome.edgesCount=graph.getEdgeCount();
		Chromosome.vertices=graph.getVertices();
		Chromosome.vertexNumber=graph.getVertexCount();
		Chromosome.edgesToColors=new int[Chromosome.edgesCount];
		setMutationRate(0.2);
	}

	void sortPopulation() {
		Collections.sort(chromosomes, new Comparator<Chromosome>() {
			public int compare(Chromosome arg0, Chromosome arg1) {
				return Integer.compare(arg0.getPenaltyFactor(),
						arg1.getPenaltyFactor());
			}
		});
	}
	
	/**
	 * Funkcja wybiera 1 liczbę r, sprawdza czy r jest mniejsze od następnej
	 * losowej liczby. Po wylosowaniu najlepszego chromosomu jest 100%
	 * prawdopodobieństwo, że zostanie wybrany, prawdopodobieństwo wybrania
	 * chromosomu zmniejsza się liniowo
	 */
	private int getRandomWithProbability() {
		int r;
		while (true) {
			r = rand.nextInt(chromosomes.size());
			if (r <= rand.nextInt(chromosomes.size())) {
				return r;
			}
		}
	}

	void crossover() {
		Chromosome ch1 = null, ch2 = null;
		ArrayList<Chromosome> newPopulation = new ArrayList<Chromosome>();
		while (newPopulation.size() < chromosomes.size()) {
			ch1=chromosomes.get(getRandomWithProbability());
			ch2=chromosomes.get(getRandomWithProbability());
			newPopulation.add(ch1.crossover(ch2));
			newPopulation.add(ch2.crossover(ch1));
		}
		chromosomes=newPopulation;
	}
	void mutate(){
		for(int i=0;i<mutationCount;i++){
			if(mutationType==0)
				chromosomes.get(getRandomWithProbability()).mutate2();
			else
				chromosomes.get(getRandomWithProbability()).mutate();
		}
	}
	
	void generateRandomPopulation() {
		for(int i=0;i<populationCount;i++){
			chromosomes.add(new Chromosome());
		}
		for (Chromosome c : chromosomes) {
			c.paintChromosome2();
		}
	}

	Chromosome bestColoring() {
		return chromosomes.get(0);
	}
	Chromosome middleColoring() {
		return chromosomes.get(populationCount/2);
	}
	
	Chromosome worstColoring() {
		return chromosomes.get(populationCount-1);
	}
	
	void setFirstPopulationColorCount(Integer count){
		Chromosome.firstPopulationColorCount=count;
	}
}
